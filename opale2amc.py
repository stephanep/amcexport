#!/usr/bin/python3
# encoding: utf-8
'''
opale2amc -- shortdesc

opale2amc is a description

@author:     Stéphane Poinsart
@copyright:  Université de Technologie de Compiègne, Cellule d'Appui Pédagogique
@license:    GPL/LGPL/CECL/MPL
@contact:    stephane.poinsart@utc.fr
'''



import sys
import os
import shutil
import argparse
import unicodedata
import re
import traceback

from lxml import etree
from macpath import dirname

#from pydev import pydevd
#pydevd.settrace("localhost", port=5678)


parser = argparse.ArgumentParser(description='Convert from Opale (xml) to Auto Multiple Choices (LaTeX)')
parser.add_argument('inputdir', help='Scenari workshop path - used to find referenced files')
parser.add_argument('inputfile', help='Root Scenari item - AMC export (best), module, quiz... as a filesystem path')
options = parser.parse_args()

ns={
    "sc":"http://www.utc.fr/ics/scenari/v3/core",
    "op":"utc.fr:ics/opale3",
    "sp":"http://www.utc.fr/ics/scenari/v3/primitive",
    }
# return element tags without python formated namespaces
def stripNs(e):
    return e.split('}', 1)[1]


parser = etree.XMLParser(remove_blank_text=True, remove_comments=True)

if (not os.path.isdir(options.inputdir)):
    sys.stderr.write('Error: "'+options.inputdir+'" inputdir is not a directory\n')
    sys.exit(1)
inputdir = os.path.realpath(options.inputdir)

if (not os.path.isfile(options.inputfile)):
    sys.stderr.write('Error: "'+options.inputfile+'" inputfile is not a file\n')
    sys.exit(1)
inputfile = os.path.realpath(options.inputfile)

if (not inputfile.startswith(inputdir)):
    sys.stderr.write('Error: "'+options.inputfile+'" is not contained in the workshop path\n')
    sys.exit(1)


out = []
genopts = {}


# question id counter (to ensure uniqueness)
qcount=0
# title id counter (to ensure uniqueness)
tcount=0
# image counter (to ensure uniqueness)
icount=0


def usableFile(filename, caller):
    filename=workpath(filename, caller)
    # check if the file we open exists
    if (not os.path.isfile(filename)):
        sys.stderr.write('Warning: "'+str(filename)+'" referenced from "'+str(caller)+'" is not a file, ignoring\n')
        #traceback.print_stack()
        return False
    
    #check if the file we open is in the real path
    filename = os.path.realpath(filename)
    if (not inputfile.startswith(os.path.realpath(inputdir))):
        sys.stderr.write('Warning: "'+str(filename)+'" referenced from "'+str(caller)+'" is not contained in the workshop path, ignoring\n')
        return False
    return True
    

# Create an ID without special char. Count has to be incremented outside of the function.
def generateId(count, text):
    if (not text):
        text='Sans Titre'
    s = ''.join(c for c in unicodedata.normalize('NFD', text) if unicodedata.category(c) != 'Mn')
    s = re.sub('[^0-9a-zA-Z]+', '-', s.strip())
    s = str(count).zfill(3) + '-' + s
    return s

# Escape characters that have no meaning in XML but are special for TeX / LaTeX
def texfilter(e):
    if (e is None):
        return None
    text=e.text
    if (text is None):
        return None
    # Dont replace when inside a math formula, we need full LaTeX processing by them
    if (not e.attrib.get('role', '   ')=='mathtex'):
        text = text.replace('\\','\\textbackslash') # backslash come first because other commands write some more
        text = text.replace('~','\\textasciitilde')
        text = text.replace('^','\\textasciicircum')
        text = text.replace('&','\\&')
        text = text.replace('%','\\%')
        text = text.replace('$','\\$')
        text = text.replace('#','\\#')
        text = text.replace('_','\\_')
        text = text.replace('{','\\{')
        text = text.replace('}','\\}')
    e.text=text


# Add some markup before / after a tag
# i.e. convert Scenari textLeaf :
# <sc:textLeaf role="math">1+1</sc:textLeaf>
# <sc:textLeaf role="math">$1+1$</sc:textLeaf>
def texmarkup(e, start, end=None):
    p=e.getparent()
    estart=etree.Element('x')
    estart.text=start
    if (not end==None):
        eend=etree.Element('x')
        eend.text=end
    p.insert(p.index(e), estart)
    if (not end==None):
        e.append(eend)

# check the image and copy it in the right path
def image(uri, caller):
    filename=os.path.basename(uri)
    base, ext = os.path.splitext(filename)
    ext=ext[1:].lower()
    
    # Most often a directory is created with the name of the file, to group meta and real data
    # So we attempt to resolve this situation by looking for (...)/filename.png/filename.png
    if (not os.path.isfile(workpath(uri, caller))):
        uri=os.path.join(uri, filename)
    
    if (not usableFile(uri, caller)):
        return None
    
    if (not ext in ['jpg', 'jpeg', 'png']):
        sys.stderr.write('Warning: "'+str(options.inputfile)+'" referenced from "'+str(caller)+'" is not a supported resource format, ignoring\n')
        return None
    
    newfilename=generateId(icount, base)+'.'+ext

    newdir=os.path.join('out','img')
    if not os.path.exists(newdir):
        os.makedirs(newdir)
    newpath=os.path.join(newdir, newfilename)

    shutil.copyfile(workpath(uri, caller), newpath)
    
    return os.path.join('img', newfilename)

# Replace all inline tags for content (question text or choice labels...)
def content(e, caller):
    global icount
    if (e is None):
        return None
    for sube in e.iter():
        texfilter(sube)
    for sube in e.iterfind(".//sc:para",ns):
        if (sube.getnext() is not None):
            texmarkup(sube, '','\n\n')
    for sube in e.iterfind(".//sc:textLeaf[@role='mathtex']",ns):
        texmarkup(sube, '$','$')
    for sube in e.iterfind(".//sc:textLeaf[@role='exp']",ns):
        texmarkup(sube, '$^{','}$')
    for sube in e.iterfind(".//sc:inlineImg[@role='ico']",ns):
        imagename=image(sube.attrib['{'+ns['sc']+'}refUri'],caller)
        if (imagename is not None):
            texmarkup(sube, '\\includegraphics[scale=0.3]{'+imagename+'}\n')
    for sube in e.iterfind(".//sp:res",ns):
        imagename=image(sube.attrib['{'+ns['sc']+'}refUri'], caller)
        if (imagename is not None):
            texmarkup(sube, '\\\\\n\\includegraphics[scale=0.3]{'+imagename+'}\n')
    for sube in e.iterfind(".//sc:textLeaf[@role='ind']",ns):
        texmarkup(sube, '$_{','}$')
    for sube in e.iterfind(".//sc:itemizedList",ns):
        texmarkup(sube, '\n\\begin{itemize}\n','\\end{itemize}\n\n')
    for sube in e.iterfind(".//sc:orderedList",ns):
        texmarkup(sube, '\n\\begin{enumerate}\n','\\end{enumerate}\n\n')
    for sube in e.iterfind(".//sc:listItem",ns):
        texmarkup(sube, '\\item{','}\n')
    textarray=[]
    for subtext in e.itertext():
        textarray.append(re.sub('\s\s\s+', ' ', subtext))
    if (textarray):
        text=''.join(textarray)
    if (text):
        return text
    else:
        return ''

# Parse MCQ (single or multiple choices) and add it's output
def parseMcq(e, inputfile, filetype):
    global qcount
    
    mult = ''
    if (filetype=='mcqMur'):
        mult = 'mult'
    
    text=content(e.find('sc:question',ns), inputfile)
    title=e.find('sp:title',ns)
    
    if (not title):
        if (text):
            qid=generateId(qcount, text[:26])
        else:
            qid=generateId(qcount, None)
    else:
        qid=generateId(qcount, title[:26])
    qcount+=1
    
    out.append('% Scenari: '+inputfile+"\n")
    out.append('\\begin{question'+mult+'}{'+qid+'}\n')
    if (text):
        out.append(text+'\n')
    out.append('\\begin{choices}\n')
    
    # For Sur (single answer), we have no right/wrong for each choice, we must get right answer index
    right = -1
    if (filetype=='mcqSur'):
        right = int(e.find('.//sc:solution',ns).attrib['choice'])

    # Iterate through choices and output them with right / wrong marker
    current=1
    for choice in e.iterfind('.//sc:choice',ns):
        validity='wrong'
        if ((filetype=='mcqMur' and choice.attrib['solution']=='checked') or current==right):
            validity='correct'
        contentchoice=content(choice.find('.//sc:choiceLabel', ns), inputfile)
        if (contentchoice):
            out.append('\t\\'+validity+'choice{'+contentchoice+'}\n')
            current+=1
    out.append('\\end{choices}\n')
    out.append('\\end{question'+mult+'}\n\n')

def str2bool(string):
    if (string==None):
        return None
    return (string=='1' or string=='True')

# Parse generic Scenari XML file with a generic function to find references to other content files recursively
def parseOther(e, inputfile, filetype):
    for ref in e.iterfind('.//*[@sc:refUri]',ns):
        newfile=ref.attrib['{'+ns['sc']+'}refUri']
        if (newfile.endswith(('.xml', '.node', '.nodelet', '.export', '.quiz', '.publi', '.case'))):
            parseFile(absolutize(newfile, inputfile), inputfile)

# parse options specific to the AMC item
def parseGenOpts(e, inputfile):
    genopts['title']=texfilter(e.find('.//op:amcExpM/sp:title',ns))
    if (genopts['title']==None):
        genopts['title']='Examen'
    
    genopts['instructions']=content(e.find('.//sp:instructions',ns), inputfile)
    if (genopts['instructions']==None):
        genopts['instructions']='\\textbf{Noircissez complètement les bonnes réponses avec un crayon très foncé ou feutre noir},\n\
         les réponses ambiguës ou les ratures ne seront pas comptabilisées. Ne redessinez pas les cases vides si vous les effacez.'
    
    genopts['namefieldname']=e.find('.//op:amcExpM/sp:namefieldname',ns)
    if (genopts['namefieldname']==None):
        genopts['namefieldname']='Nom :'
    
    genopts['studentnumbersize']=e.find('.//op:amcExpM/sp:studentnumbersize',ns)
    if (genopts['studentnumbersize']==None):
        genopts['studentnumbersize']=5
    
    genopts['signaturefield']=str2bool(e.find('.//op:amcExpM/sp:signaturefield',ns))
    if (genopts['signaturefield']==None):
        genopts['signaturefield']=True
    
    genopts['font']=e.find('.//op:amcExpM/sp:font',ns)
    if (genopts['font']==None):
        genopts['font']='Linux Libertine O'
    
    genopts['randomquestion']=str2bool(e.find('.//op:amcExpM/sp:randomquestion',ns))
    if (genopts['randomquestion']==None):
        genopts['randomquestion']=True
    
    genopts['randomchoice']=str2bool(e.find('.//op:amcExpM/sp:randomchoice',ns))
    if (genopts['randomchoice']==None):
        genopts['randomchoice']=True
    
    genopts['answersheet']=str2bool(e.find('.//op:amcExpM/sp:answersheet',ns))
    if (genopts['answersheet']==None):
        genopts['answersheet']=True
    
    genopts['err1']=e.find('.//op:amcExpM/sp:scoring/sp:err1',ns)
    if (genopts['err1']==None):
        genopts['err1']='0'
    
    genopts['err2']=e.find('.//op:amcExpM/sp:scoring/sp:err2',ns)
    if (genopts['err2']==None):
        genopts['err2']=genopts['err1']
    
    genopts['errmore']=e.find('.//op:amcExpM/sp:scoring/sp:errmore',ns)
    if (genopts['errmore']==None):
        genopts['errmore']=genopts['err2']


# Transform a workshop path to a filesystem path
def workpath(path, caller):
    # absolute path : just remove the /
    if (path.startswith('/')):
        return(os.path.join(inputdir, path[1:]))
        
    # relative path : join with the caller path, if we have one
#    if ((not caller) or (not os.path.dirname(caller))):
        #return os.path.join(inputdir, path)
    
    callerdir=os.path.dirname(caller)
    if callerdir.startswith('/'):
        callerdir=callerdir[1:]
#    print('workpath: path='+path +', caller=' + caller)
#    print('workpath: ret='+os.path.normpath(os.path.join(inputdir, callerdir, path)))
#    traceback.print_stack()
    return os.path.normpath(os.path.join(inputdir, callerdir, path))

# Transform a relative workshop path to an absolute workshop path 
def absolutize(path, caller):
    fspath=workpath(path, caller)
    fspath=os.path.normpath(fspath)
#    print('absolutize -> path='+path+', caller='+caller);
#    print('absolutize -> return '+os.path.relpath(fspath, inputdir));
    return '/'+os.path.relpath(fspath, inputdir)

# Load and parse any XML file and route it to the appropriate handler (XML or quiz)
def parseFile(inputfile, caller=None):
    if (caller is None):
        inputfile='/'+os.path.relpath(inputfile, inputdir)
    
    effectivefile=workpath(inputfile, caller)
    
    if (caller and not usableFile(inputfile, caller)):
        return
    
    tree = etree.parse(effectivefile, parser)
    if (not tree):
        print('Error: XML from "'+effectivefile+'" not loaded correctly (parse)')
        return
    root = tree.getroot()
    if (not tree):
        print('Error: XML from "'+effectivefile+'" not loaded correctly (getroot)')
        return
    
    filetype = stripNs(root[0].tag)
    
    # first file / empty options
    if (not genopts):
        parseGenOpts(root[0], inputfile)
    
    # supported quiz type
    if (filetype=='mcqSur' or filetype=='mcqMur'):
        parseMcq(root[0], inputfile, filetype)
    # other XML file that is not a quiz
    elif (not inputfile.endswith('.quiz')):
        parseOther(root[0], inputfile, filetype)


# Prepare output file
os.makedirs('out')
parseFile(inputfile)
outfile=open('out/out.tex','a', encoding="utf-8")

outfile.write('\
% ===========================\n\
% = Paramétrage de l\'examen =\n\
% ===========================\n\
\n\
% Vous pouvez modifier ces réglages dans Scenari, ou directement ci-dessous pour faire varier facilement les modalités\n\
\n\
% Titre du sujet d\'examen\n\
\\newcommand\scenarititle{'+genopts['title']+'}\n\
\n\
% Les consignes sont indiquées en début de sujet pour que les étudiants du test comprennent son déroulement.\n\
% Donner des infos sur la durée, les documents autorisés, le barème, le remplissage des cases,\n\
% la possibilité de cocher plusieurs cases à la même question...\n\
\\newcommand\scenariinstructions{'+genopts['instructions']+'}\n\
\n\
% L\'étudiant doit rentrer une information dans une case pour vérifier son identification.\n\
% Par défaut "Nom :". Si vous préférez vous pouvez pour anonymiser mettre "Numéro d\'étudiant :"\n\
\\newcommand{\scenarinamefieldname}{'+genopts['namefieldname']+'}\n\
\n\
% Vous pouvez demander à l\'étudiant de signer sa feuille si vous voulez le responsabiliser sur le fait qu\'il ne doit pas chercher à utiliser une fausse identité.\n\
\\newcommand{\scenarisignaturefield}{'+str(int(genopts['signaturefield']))+'}\n\
\n\
% Nombre de chiffres dans le numéro d\'identification de l\'étudiant\n\
\\newcommand\scenaristudentnumbersize{'+str(int(genopts['studentnumbersize']))+'}\n\
\n\
% Cette police doit être installée sur l\'ordinateur qui fait la génération XeTeX.\n\
% Elle doit supporter les caractères spéciaux qui pourraient être utilisées dans vos questions\n\
% Autres choix fiables : Times New Roman, DejaVu Serif, DejaVu Sans\n\
\\newcommand{\scenarifont}{'+genopts['font']+'}\n\
\n\
% ordre aléatoire des questions entres elles\n\
\\newcommand{\scenarirandomquestion}{'+str(int(genopts['randomquestion']))+'}\n\
\n\
% ordre des choix aléatoires au sein d\'une question\n\
\\newcommand{\scenarirandomchoice}{'+str(int(genopts['randomchoice']))+'}\n\
\n\
% Grille de réponse séparé : plutôt que d\'écrire directement sur le sujet,\n\
% l\'étudiant complète et restitue uniquement une grille de réponse sur une page séparée\n\
\\newcommand{\scenarianswersheet}{'+str(int(genopts['answersheet']))+'}\n\
%\n\
% Tout bon = 1 point, pas rempli ou invalide = 0 points\n\
% Points à donner pour 1 erreur\n\
\\newcommand{\scenarierra}{'+str(genopts['err1'])+'}\n\
\n\
% Points à donner pour 2 erreurs\n\
\\newcommand{\scenarierrb}{'+str(genopts['err2'])+'}\n\
\n\
% Points à donner pour plus de 2 erreurs\n\
\\newcommand{\scenarierrmore}{'+str(genopts['errmore'])+'}\n\
\n\
% ===========================\n\
\n\n')

scriptdir=os.path.dirname(os.path.abspath(__file__))

# Write header
headerfile=open(os.path.join(scriptdir,'header.tex'),'r', encoding="utf-8")
for line in headerfile.readlines():
    outfile.write(line)
headerfile.close

# Write content
outfile.write(''.join(out))

# Write footer
footerfile=open(os.path.join(scriptdir,'footer.tex'),'r', encoding="utf-8")
for line in footerfile.readlines():
    outfile.write(line)
footerfile.close

outfile.close

