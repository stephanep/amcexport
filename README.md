# amcexport

Scripts to convert Scenari XML MCQ files to Auto Multiple Choice LaTeX files.

## Package content 

/ : pyhon conversion scripts
/web/ : web launcher for the script
/scenari-model/ : SCENARIbuilder files with aditional metadata to tweak the export settings (optional, can be changed in the generated LaTeX content) 


## Basic install

The script may be tweaked to run on other OS but it has only be tested on Linux :

    cd /opt
    git clone "https://framagit.org/stephanep/amcexport" scenari2amc
    apt-get install python3-lxml

For servers without auto-multiplechoice already installed, add :

    apt-get install --no-install-recommends auto-multiple-choice-common

Other package may be required


## Basic usage

opale2amc.py inputdir inputfile

* inputdir : Scenari workshop path - used to find referenced files
* inputfile : Root Scenari item - AMC export (best), module, quiz... as a filesystem path

Example :

    /opt/scenari2amc/opale2amc.py mycontent mycontent/myspace/amc.xml

With "mycontent" the folder containing the Scenari / Opale source of your workshop,
and "myspace/amc.xml" the XML file that reference some MCQ. 


Output will be generated in the "out" directory, created in the current directory. Script will stop
if it already exists to avoid important content beeing overwriten.


## Web frontend

It's a simple php script

Make sure you extract the script outside of the webroot folder (i.e. /opt/scenari2amc/ )
and then link only the web folder to your webroot (i.e. : ln -s /opt/scenari2amc/web /var/www/scenari2amc )

Create a new writable directory :

    mkdir /opt/scenari2amc/web/upload
    chown www-data.www-data /opt/scenari2amc/web/upload


Create a cron cleanup to remove old files in /tmp/upload and /opt/scenari2amc/web/upload to avoid keeping
user data too long.

chroot should be used to improve security but has not been integrated to the webpage, so it's provided without
any security waranty. LaTeX is the most risky component, new LaTeX version prevent some of the problems related
to including files outside of the current directory. Dont use old LaTeX versions (<2016).


## Scenari model

Create a link from "scenari-model" to "/amc" in a SCENARIbuilder workshop with the Opale 3.6.100 model.
	
