<?php
function getRandomString($length){
	$chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
	$result = '';
	while(strlen($result)<$length) {
		$result .= $chars{mt_rand(0,strlen($chars)-1)};
	}
	return $result;
}

function error($text) {
	require_once("header.inc.php");
	echo($text);
	require_once("footer.inc.php");
	exit(1);
}


// function from http://stackoverflow.com/a/834355/3104853
function endswith($haystack, $needle)
{
	$length = strlen($needle);
	if ($length == 0) {
		return true;
	}
	
	return (substr($haystack, -$length) === $needle);
}


// look in a directory recursively to find a file that contains "$filename"
function findfile($dir, $filename) {
	$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir), RecursiveIteratorIterator::SELF_FIRST);
	
	foreach ($iterator as $path) {
		if (!$path->isDir()) {
			$pathstr=$path->__toString();
			if (strpos($pathstr, $filename) !== false) {
				return($pathstr);
			}
		}
	}
	return false;
}


function printlogs($cmdout) {
	if (!count($cmdout) || !trim($cmdout[0]))
		return;
	echo '<pre class="logs">';
	foreach ($cmdout as $cmdoutline)
		echo $cmdoutline. "\n";
	echo "</pre>";
}

if (!empty($_FILES)) {
	$id=getRandomString(16);
	$pathroot = '/tmp/upload/'.$id.'/';
	$pathin = $pathroot . 'in/';
	$pathout = $pathroot. 'out/';
	if (!file_exists($pathroot))
		mkdir($pathroot, 0700, true);
	if (!file_exists($pathin))
		mkdir($pathin, 0700, true);
	$filein = $pathroot . "scenari.scar";
	$fileout = $pathroot . "latex.zip";
	$pathfinal = __DIR__ . '/upload/'.$id.'/';
	if (!file_exists($pathfinal))
		mkdir($pathfinal, 0700, true);
	
	if (!move_uploaded_file($_FILES['file']['tmp_name'], $filein)) {
		error("Erreur interne : le fichier envoyé n'a pu être chargé correctement");
	}
	
	// unzip the uploaded file
	$zip = new ZipArchive;
	$res = $zip->open($filein);
	if ($res === TRUE) {
		$zip->extractTo($pathin);
		$zip->close();
	} else {
		error("erreur : le fichier envoyé n'a pas pu être dézippé");
	}
	
	// find the root filepath (amc.*)
	$fileroot=findfile($pathin, '/amc.');
	if (!$fileroot) {
		error('erreur : impossible de trouver le fichier "amc.xml" dans votre fichier scar');
	}
	// path is relative to the workshop root and not an absolute path
	// strip the first part of the path
	$fileroot='in/'.substr($fileroot,strlen($pathin));
	
	chdir($pathroot);
	require_once("header.inc.php");

	echo "fichier accepté... Traitement en cours\n\n";
	exec("python3 /opt/scenari2amc/opale2amc.py $pathin $fileroot 2>&1", $cmdout, $errcode);
	if ($errcode===0 && file_exists('out/out.tex') && filesize('out/out.tex')>128) {
		echo "<br><b>Conversion terminée !</b><br>";
		printlogs($cmdout);
	} else {
		printlogs($cmdout);
		
		error("<br><b>Erreur lors de la conversion !</b><br>");
	}
	
	
	exec("zip -r latex.zip out 2>&1", $cmdout, $errcode);
	if ($errcode===0 && file_exists('latex.zip') && filesize('latex.zip')>128) {
		rename($pathroot.'latex.zip', $pathfinal.'latex.zip');
		echo "<p><a href=\"./upload/$id/latex.zip\">Téléchargez vos fichiers LaTeX</a></p>";
	} else {
		echo '<pre>';
		printlogs($cmdout);
		error("erreur dans la production du fichier zip de contenus");
		echo '</pre>';
	}
	chdir('out');
	
	exec("xelatex out.tex 2>&1", $cmdout, $errcode);
	if ($errcode===0 && file_exists('out.pdf') && filesize('out.pdf')>2048) {
		rename($pathroot.'out/out.pdf', $pathfinal.'out.pdf');
		echo "<p>Prévisualisation : <br><iframe width=\"800\" height=\"900\" src=\"./upload/$id/out.pdf\"><a href=\"./upload/$id/out.pdf\">Lien de prévisualisation PDF</a></iframe></p>";
		printlogs($cmdout);
	} else {
		printlogs($cmdout);
		error("erreur dans la production de la prévisualisation");
	}
		
	
}

require_once('header.inc.php');

?>
<h2>Objectifs</h2>
<p>
	Cet outil permet de convertir des QCM / QCU Scenari en un sujet d'examen Auto Multiple Choice.
	Les exercices doivent être au format Opale, et listés dans un item ou un réseau d'items Opale ou de modèles dérivés d'Opale
	(topaze, rubis, faq2sciences...). Il fonctionne bien avec les exercices qui comportent des formules LaTeX et des images (png, jpg)
</p>
<h2>Procédure</h2>
<p>
<ul>
	<li>
		Créez dans vos contenus un item "amc.xml" (liste d'exercice, module...) et faites référence à vos exercices dans ce module.
		<img/>
	</li>
	<li>
		Exportez une archive de cet item, en gardant les options par défaut (inclure le réseau descendant...)
		<img/>
	</li>
	<li>
		Chargez votre fichier .scar dans l'emplacement ci-dessous.<br>
		<form action="?" method="post" enctype="multipart/form-data">
			<label for="file">.scar Opale (ou modèle dérivé) :</label>
			<input type="hidden" name="MAX_FILE_SIZE" value="262144000"/>
			<input type="file" id="file" name="file"/>
			<input type="submit" value="Envoyer" name="submit" id="submit"/>
		</form>
	</li>
	<li>
		Vérifiez sur la prévisualisation que le sujet de test est généré correctement.
		 
	</li>
	<li>
		Modifiez le début du fichier tex pour avoir les réglages qui vous conviennent (mélange des questions/choix, feuille de réponse séparée...)
		et générez le par AMC pour obtenir la version finale que vous pouvez imprimer. Cette version ne fonctionne qu'avec
		<a href="http://auto-multiple-choice.net/download.fr">AMC en version 1.3</a>. Si vous constatez des défauts sur la position des cases avec AMC 1.2
		mettez à jours vers une version plus récente.
	</li>
</ul>
</p>
<p>
	Attention : la sécurité de cette première version du service n'est pas adapté à la conversion de documents confidentiels.
	Vous pouvez installer les scripts de conversion sur votre propre ordinateur (<a href="https://framagit.org/stephanep/amcexport" target="_blank">sources</a>).
</p>
<p>
	En cas d'erreur, vous pouvez <a href="mailto:stephane.poinsart@utc.fr">contacter l'administrateur de ce service de conversion</a>.
</p>


<?php
require_once('footer.inc.php');
?>
